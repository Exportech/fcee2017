$(function(){


$(document).ready(function(){

/* section Accueil */

$('#compteurJours').text('jours');

$(".compteur")
  .countdown("2016/10/20 12:00:00", function(event) {
  $('h3').first().text(
      event.strftime("%D")

  );
  $('h3').eq(1).text(
      event.strftime("%H")
  );
  $('h3').eq(2).text(
      event.strftime("%M")

  );
  $('h3').eq(3).text(
      event.strftime("%S")

  );
  });


  // resize fenetre
                         var hauteur_fenetre = $(window).height();
                                $('.compteur').css('margin-top', (hauteur_fenetre)/30);

                                $(window).resize(function(){
                                var hauteur_fenetre = $(window).height();
                                $('.compteur').css('margin-top', (hauteur_fenetre)/30);
                                               });


                               $('.navbar-collapse li').on('click', function () {

                              		if (window.innerWidth < 768) {
                             			$('.navbar-collapse').removeClass('in');
                             		}
                             	});

/* fin section Accueil */

/* section Foire */
/* fin section Foire */

/* section Forfaits */

// traduction du site
var translation, translate;
translation = {

    "Fr": {

        /* section Accueil */
        "#bienvenue": "Bienvenue à la 19e Foire canadienne des entreprises d'entraînement 2016",
        "#boutonAccueil": "Accueil",
        "#boutonFoire": "Foire",
        "#boutonForfaits": "Forfaits",
        "#boutonContact": "Contact",
        "#compteurJours": "jours",
        "#compteurHeures": "heures",
        "#compteurMinutes": "min",
        "#compteurSecondes": "sec",
        "#compteurPrésentation": "Le temps file, faites-vous plaisir!",

        /* section Foire */
        "#foire h1": "Foire",
        "#foire h2": "Nous sommes là où vous êtes !",
        "#foire h3": "Exportech Québec",
        "#foireLigne1": "L'apprentissage par le travail et l'action!",
        "#foireLigne2": "Nous sommes une entreprise d'entraînement qui simule une gestion administrative et des activités commerciales notamment des produits et services touristiques pour la promotion de la Ville de québec.",

        /* section Forfaits */
        "#forfaits>h1": "Forfaits",
        "#forfaits>h2": "La vie est sujette à bien des aventures, soyons-en les artisans!",

        /* titre et description des forfaits */
        "h3.glace": "Forfait 1",
        "h4.glace": "Glace",
        "p.glace": "Gâtez-votre famille et vivez simultanément des vacances estivales et hivernales au Québec.",
        "#glaceDescLigne1": "Venez glisser une journée en maillot de bain et en habit de neige dès le lendemain. Notre Forfait Glace inclut aussi une nuit bien au chaud à l’hôtel Village Val-Cartier et une visite rafraîchissante de l’Hôtel de Glace situé juste à côté.",
        "#glaceDescLigne2": "Réservez dès maintenant votre week-end chaleur et fraîcheur !",
        "#glaceLigne1": "2 nuitées à l'hôtel Village Vacances Val-Cartier",
        "#glaceLigne2": "2 déjeuners continentaux",
        "#glaceLigne3": "1 journée neige-rafting",
        "#glaceLigne4": "1 buffet en soirée",
        "#glaceLigne5": "1 visite de l’Hôtel de Glace avec cocktail dans un verre de glace",
        "#glaceLigne6": "1 journée au parc aquatique intérieur",

        "h3.aqualumiere": "Forfait 2",
        "h4.aqualumiere": "Aqua-lumière",
        "p.aqualumiere": "Découvrez la surprenante beauté de l’Aquarium du Québec lors d’une soirée Festi-Lumière.",
         "#aqualumiereDescLigne1": "Tout au long de votre parcours, laissez-vous éblouir par les nombreuses silhouettes colorées et lumineuses. Après la visite, profitez d’une nuitée en famille dans le grand confort de l’hôtel Ambassadeur.",
         "#aqualumiereLigne1": "1 nuitée à l’hôtel Ambassadeur",
         "#aqualumiereLigne2": "Accès piscine, spa et jardin intérieur",
         "#aqualumiereLigne3": "Soirée « Festi-Lumière » à l’Aquarium du Québec",

        "h3.ski": "Forfait 3",
        "h4.ski": "Relax-à-ski",
        "p.ski": "Profitez au maximum des nombreux plaisirs de l’hiver à Stoneham.",
        "#skiDescLigne1": "Venez skier une journée, relaxer à l’Halte-O-Spa et souper au restaurant 4 Foyers. Vous finirez en marchant tranquillement jusqu’à votre chambre située directement sur le site. Le lendemain, vous déjeunez et recommencez à skier.",
        "#skiLigne1": "1 nuitée à l’hôtel Stoneham",
        "#skiLigne2": "2 jours de ski alpin : Expérience « L’inséparable » à l’Halte-O-Spa pour 2 personnes",
        "#skiLigne3": "1 table d’hôte au restaurant 4 Foyers",

        "h3.adrenaline": "Forfait 4",
        "h4.adrenaline": "Adrénaline",
        "p.adrenaline": "Si vous êtes amateur de course à pied et de films d’horreur, c’est l’activité parfaite pour vous.",
        "#adrenalineDescLigne1": "Venez tenter d’échapper aux attaques des zombies que vous rencontrerez tout au long d’un parcours de 5 km. Ils tenteront par tous les moyens de vous empêcher de festoyer au banquet postapocalyptique offert à la fin.",
        "#adrenalineDescLigne2": "Terminerez-vous la course en vie ou en zombie ?",
        "#adrenalineLigne1": "Traversez des lieux surréalistes aux ambiances dignes d’un film d’horreur.",
        "#adrenalineLigne2": "Banquet postapocalyptique avec DJ sur les Plaines d’Abraham",

        "h3.folklore": "Forfait 5",
        "h4.folklore": "Folklore",
        "p.folklore": "Partez en raquettes au flambeau à la rencontre d’un trappeur qui vous racontera la légende de la Chasse-Galerie.",
        "#folkloreDescLigne1": "Sous un ciel étoilé, il vous fera signer un pacte avec le diable, que vous devrez absolument respecter. Si vous manquez à votre parole, vous ne dégusterez peut-être pas le vin et fromage prévu à la fin du parcours.",
        "#folkloreLigne1": "Aventure en raquettes au flambeau à la Vallée Bras-du-Nord",
        "#folkloreLigne2": "Rencontrez un trappeur qui vous raconte la légende de la Chasse-Galerie.",
        "#folkloreLigne3": "Boissons chaudes, vin et fromage offerts",

        "h3.musee": "Forfait 6",
        "h4.musee": "Resto Musée",
        "p.musee": "Vous avez rendez-vous avec la culture et la gastronomie.",
        "#museeDescLigne1": "Nourrissez votre esprit en visitant le Musée national des Beaux–Arts du Québec et ensuite votre estomac en vous régalant d’une « Dégustation gourmande » au Bistro 1640.",
        "#museeDescLigne2": "Ce forfait sera sûrement à votre goût !",
        "#museeLigne1": "Accès au Musée national des Beaux-Arts du Québec",
        "#museeLigne2": "1 nuitée à l’Auberge du Trésor",
        "#museeLigne3": "Dégustation gourmande au Bistro 1640",

        "h3.carnaval": "Forfait 7",
        "h4.carnaval": "Carnaval",
        "p.carnaval": "Participez au Carnaval de Québec en vivant la vie de château.",
        "#carnavalDescLigne1": "Vous serez en plein cœur des festivités carnavalesques en logeant deux nuits au prestigieux Château Frontenac. Pendant votre séjour, vous assisterez au souper du Bonhomme et à son brunch, en plus d’avoir accès à toutes les activités extérieures en portant fièrement votre effigie.",
        "#carnavalDescLigne2": "Bon Carnaval !",
        "#carnavalLigne1": "2 nuitées au Château Frontenac",
        "#carnavalLigne2": "1 laissez-passer (effigie) aux activités du Carnaval de Québec",
        "#carnavalLigne3": "1 place au souper du Carnaval",
        "#carnavalLigne4": "1 brunch du Carnaval",

        /* tarification */
        "th.prixAdulte": "Adulte",
        "th.prixEnfant": "Enfant",
        "th.prixFamille": "Famille",
        "th.prixPersonne": "Personne",
        ".prixNote": "Famille = deux adultes et deux enfants",

        /* autres textes */
        "#forfaits>p:last-child": "Pour usage pédagogique seulement",

        /* section Tirage */
        "#tirage>h1": "Tirage",
        "#tirage>h2": "Achetez du virtuel et vivez le réel!",
        "#tirageHotel": "Gagnez une nuitée à l'hôtel Fairmont Château Frontenac ou à l'hôtel Le Concorde",
        "#tirageBillets": "Avec deux passeports VIP pour visiter la ville de Québec ou deux billets de ski",

        /* section Contact */
        "#contact>h1": "Nous contacter",
        "#contact>h2": "Pour de plus amples informations",

        /* coordonnées */
      	'#coordLigne1': "1530 boulevard de l'Entente",
      	'#coordLigne2': 'Québec (Québec) G1S 4S7' ,
      	'#coordLigne3': 'T 418 686 0232',
      	'#coordLigne4': 'expinfo@exportechquebec.ca',

        /* demande information */
        // '#infoPrenom.attr(value)': 'Prénom',
        // '#infoNom.attr(value)': 'Nom',
        // '#infoMessage.attr(value)': 'Message',
        // '#infoEnvoyer.attr(value)': 'Envoyer',

        /* section Footer */
        "#footer>h1": "Merci à nos commanditaires"
      },

    "En": {

      /* section Accueil */
      "#bienvenue": "Welcome to 19th Fair Canadian Practice Firms 2016",
      "#boutonAccueil": "Home",
      "#boutonFoire": "Fair",
      "#boutonForfaits": "Package",
      "#boutonContact": "Contact",
      "#compteurJours": "days",
      "#compteurHeures": "hours",
      "#compteurMinutes": "min",
      "#compteurSecondes": "sec",
      "#compteurPrésentation": "Time flies, have fun!",

      /* section Foire */
      "#foire h1": "Fair",
      "#foire h2": "We are where you are!",
      "#foire h3": "Exportech Québec",
      "#foireLigne1": "Learning through work and action!",
      "#foireLigne2": "We're a practice firm simulates an administrative and commercial activities including tourism products and services for the promotion of Quebec City.",

      /* section Forfaits */
      "#forfaits>h1": "Package",
      "#forfaits>h2": "Life is subject to many adventures let in artisans!",

      /* titre et description des forfaits */
      "h3.glace": "Package 1",
      "h4.glace": "Glace",
      "p.glace": "Treat your family and live simultaneously summer and winter holidays in Quebec.",
      "#glaceDescLigne1": "Come and slip in a swimsuit during the day and in snow suit the next day. Our Ice package also includes a very warm night at the hotel Village Valcartier and a refreshing visit of the Ice Hotel located next door.",
      "#glaceDescLigne2": "Book now your weekend “warmth and freshness” !",
      "#glaceLigne1": "2 nights at the hotel Village Vacances Val-Cartier",
      "#glaceLigne2": "2 continental breakfasts",
      "#glaceLigne3": "1 day of snow-rafting",
      "#glaceLigne4": "1 evening-buffet",
      "#glaceLigne5": "1 visit of the Ice Hotel with a cocktail drink served in an ice-made glass",
      "#glaceLigne6": "1 day at the indoor aquatic park",

      "h3.aqualumiere": "Package 2",
      "h4.aqualumiere": "Aqua Lumière",
      "p.aqualumiere": "Discover the amazing beauty of the Aquarium du Québec during a Festi-Lumière evening.",
       "#aqualumiereDescLigne1": "Be dazzled by the many colorful and bright silhouettes throughout your journey. After the tour, enjoy an overnight accommodation with your family in the great comfort of the hotel Ambassador.",
       "#aqualumiereLigne1": "1 night at the hotel Ambassadeur",
       "#aqualumiereLigne2": "Access to a swimming-pool, a spa and an interior garden",
       "#aqualumiereLigne3": "A “Festi-Lumière” evening at the Aquarium du Québec",

      "h3.ski": "Package 3",
      "h4.ski": "Relax-à-ski",
      "p.ski": "Make the most of the many pleasures of winter in Stoneham.",
      "#skiDescLigne1": "Come to ski one day, relax at the Halte-O-Spa and dine at the restaurant 4 Foyers. You will finish wandering up to your room directly on the site. The next day, take your lunch and start to ski !",
      "#skiLigne1": "1 night at the hotel Stoneham",
      "#skiLigne2": "2 days of alpine skiing: Experience “L’inséparable” at Halte-O-Spa for 2 persons",
      "#skiLigne3": "1 table d’hôte at the restaurant 4 Foyers",

      "h3.adrenaline": "Package 4",
      "h4.adrenaline": "Adrénaline",
      "p.adrenaline": "If you love running and horror movie, this is the perfect activity for you.",
      "#adrenalineDescLigne1": "Come and try to escape the attacks of zombies you encounter along a 5 km trail.",
      "#adrenalineDescLigne2": "They will try by all means to prevent you from feast to postapocalyptic banquet at the end.",
      "#adrenalineLigne1": "Go through surreal places in an atmosphere worthy of horror movies.",
      "#adrenalineLigne2": "A postapocalyptic banquet with a DJ on the Plaines d’Abraham",

      "h3.folklore": "Package 5",
      "h4.folklore": "Folklore",
      "p.folklore": "Go snowshoeing with torchlight to meet a trapper who tells you the legend of Flying Canoe.",
      "#folkloreDescLigne1": "Under a starry sky, he will make you sign a pact with the devil that you will absolutely comply. If you do not keep your words, you may not perhaps enjoy wine and cheese at the end of the course.",
      "#folkloreLigne1": "Torch-lit adventure on snowshoes at the Vallée Bras-du-Nord",
      "#folkloreLigne2": "Meet a trapper who will tell you the legend of the Flying Canoe.",
      "#folkloreLigne3": "Hot beverages, wine and cheese served",

      "h3.musee": "Package 6",
      "h4.musee": "Resto Musée",
      "p.musee": "You have an appointment with the culture and gastronomy.",
      "#museeDescLigne1": "Feed your spirit by visiting the Musée national des Beaux–Arts du Québec and then your stomach by feasting with the “Dégustation gourmande” at Bistro 1640.",
      "#museeDescLigne2": "This package will surely be for your taste !",
      "#museeLigne1": "Admission to the Musée national des Beaux-Arts du Québec",
      "#museeLigne2": "1 night at the Auberge du Trésor",
      "#museeLigne3": "Gourmet tasting at the Bistro 1640",

      "h3.carnaval": "Package 7",
      "h4.carnaval": "Carnaval",
      "p.carnaval": "Participate to the Carnaval de Québec by living in a castle.",
      "#carnavalDescLigne1": "You will be in the heart of the carnival festivities by staying two nights at the prestigious Château Frontenac. During your stay, you will attend the supper of Bonhomme carnaval and its brunch, in addition to having access to all outdoor activities by wearing proudly your effigy.",
      "#carnavalDescLigne2": "Good Carnival !",
      "#carnavalLigne1": "2 nights at the Château Frontenac",
      "#carnavalLigne2": "1 pass (effigy) to all the events of the Carnaval de Québec",
      "#carnavalLigne3": "1 place at the Carnival’s dinner",
      "#carnavalLigne4": "1 Carnival’s brunch",

      /* tarification */
      "th.prixAdulte": "Adult",
      "th.prixEnfant": "Kid",
      "th.prixFamille": "Family",
      "th.prixPersonne": "Person",
      ".prixNote": "Family = two adults and two children",

      /* autres textes */
      "#forfaits>p:last-child": "Not for real selling",

      /* section Tirage */
      "#tirage>h1": "Prize draw",
      "#tirage>h2": "Buy virtual and live real!",
      "#tirageHotel": "One night at the Château Frontenac with 2 VIP passports",
      "#tirageBillets": "One night at the hotel Le Concorde with 2 Ski tickets at Stoneham",

      /* section Contact */
      "#contact>h1": "Contact us",
      "#contact>h2": "For more information",

      /* coordonnées */
      '#coordLigne1': "1530 boulevard de l'Entente",
      '#coordLigne2': 'Québec (Québec) G1S 4S7' ,
      '#coordLigne3': 'T 418 686 0232',
      '#coordLigne4': 'expinfo@exportechquebec.ca',

      /* demande information */
      // '#infoPrenom': 'First name',
      // '#infoNom': 'Last name',
      // '#infoMessage': 'Message',
      // '#infoEnvoyer': 'Send',

      /* section Footer */
      "#footer>h1": "Thanks!"
    }
};

translate = function( lng,translationObj ){
    if( lng && translationObj[lng] ) {
        return $.each(translationObj[lng], function(k,v){
             $(k).text(v);
        });
    }else{
        return $.error("arrete voir");
    }
};

$(".lng a").bind("click", function(event){
    event.preventDefault();

    translate( $(this).text(), translation);

    //translate("fr", translation);
});

// affichage des informations spécifiques des forfaits
$('#forfaits article').unbind('click').bind('click', function(event) {
  //identification classe article cliqué
	var choixForfait = $(this).attr('class');
	//récupération des descriptions de forfait
	var listeForfaits = $('div.description');
	var longueur = $('div.description').length;
  //forfait choisi: afficher/cacher la description (tous les éléments de la div correspondante)
	//autres forfaits: cacher la description si besoin (:visible), sinon rien
	for (var i = 0; i <= longueur-1; i++) {
    var position = $(listeForfaits[i]).attr('id');
    var nameClass = $(this).children().children().attr('class');
    if (nameClass == position) {
        if ($(listeForfaits[i]).is( ":visible" )){
            $(this).removeClass('blanc');
            $(listeForfaits[i]).fadeToggle();
        }
        else{
            $(this).addClass('blanc');
            $(listeForfaits[i]).fadeToggle();
        }
		} else {
			//  if ($(listeForfaits[i]).is( ":visible" )) {
      //    $(listeForfaits[i]).parent().removeClass('blanc');
      //    $(listeForfaits[i]).fadeToggle();
			// }
		}
	};
});

/* modification des fichiers de prix des forfaits selon la langue choisie */
$('.lng>a').click(function(){
   var choixLangue = $(this).attr('id');
   if (choixLangue == "français") {
     $('#prix1').attr('src', 'images/forfait_1_fr.png');
     $('#prix2').attr('src', 'images/forfait_2_fr.png');
     $('#prix3').attr('src', 'images/forfait_3_fr.png');
     $('#prix4').attr('src', 'images/forfait_4_fr.png');
     $('#prix5').attr('src', 'images/forfait_5_fr.png');
     $('#prix6').attr('src', 'images/forfait_6_fr.png');
     $('#prix7').attr('src', 'images/forfait_7_fr.png');
   } else {
     $('#prix1').attr('src', 'images/forfait_1_ang.png');
     $('#prix2').attr('src', 'images/forfait_2_ang.png');
     $('#prix3').attr('src', 'images/forfait_3_ang.png');
     $('#prix4').attr('src', 'images/forfait_4_ang.png');
     $('#prix5').attr('src', 'images/forfait_5_ang.png');
     $('#prix6').attr('src', 'images/forfait_6_ang.png');
     $('#prix7').attr('src', 'images/forfait_7_ang.png');
   };
});


/* fin section Forfaits */

/* section Tirage */
/* fin section Tirage */

/* section Contact */
/* fin section Contact */

/* section Footer */
/* fin section Footer */

});
});
